DEVELOPMENT ?= docker-compose-dev.yml
PRODUCTION ?= docker-compose-pro.yml


bup:
	docker-compose -f $(ENV) build && \
	 docker-compose -f $(ENV) stop && \
	 docker-compose -f $(ENV) rm -f && \
	 docker-compose -f $(ENV) up -d && \
	 docker system prune --force

pro: ENV:=$(PRODUCTION)
pro: bup

dev: ENV:=$(DEVELOPMENT)
dev: bup


migrate:
	docker-compose -f $(ENV) exec web /venv/bin/python manage.py migrate

migratepro: ENV:=$(PRODUCTION)
migratepro: migrate

migratedev: ENV:=$(DEVELOPMENT)
migratedev: migrate


collectstatic:
	docker-compose -f $(ENV) exec web /venv/bin/python manage.py collectstatic

collectpro: ENV:=$(PRODUCTION)
collectpro: collectstatic

collectdev: ENV:=$(DEVELOPMENT)
collectdev: collectstatic


mm:
	(cd sources && python manage.py makemigrations)

mu:
	docker-compose exec web python manage.py migrate