#!/bin/sh

curl -X POST https://content.dropboxapi.com/2/files/upload \
    --header "Authorization: Bearer 4XHDqAK4yQkAAAAAAAAneRVRGvq7G7zB8IYlHh6V0p_RbTTCxNMr0AKVwoudXDCt" \
    --header "Dropbox-API-Arg: {\"path\": \"/backups-$PROJECT_NAME/$1\"}" \
    --header "Content-Type: application/octet-stream" \
    --data-binary @/backups/$1

