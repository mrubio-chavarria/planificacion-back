import inspect
import json
from functools import wraps

from django.core.exceptions import PermissionDenied


def months(start_month, start_year, end_month, end_year):
    return (((m_y % 12) + 1, m_y / 12) for m_y in
            range(12 * start_year + start_month - 1, 12 * end_year + end_month))


def dumps(value):
    return json.dumps(value,default=lambda o:None)


def require_ajax(view):
    @wraps(view)
    def _wrapped_view(request, *args, **kwargs):
        if request.is_ajax():
            return view(request, *args, **kwargs)
        else:
            raise PermissionDenied()

    return _wrapped_view


def get_request_from_stack():
   """
   This works because of django call stack nature.
   All the signals are called syncronously after model save or delete operations are done
   so we can inspect the call stack looking for the request object
   Note: This won´t work if we introduce some threading or aysnc operations
   """
   for frame_record in inspect.stack():
       if frame_record[3] == 'get_response':
           return frame_record[0].f_locals['request']
   return None
