from collections import namedtuple

from rest_framework import serializers


class ManyRelatedField(serializers.ManyRelatedField):
    def __init__(self, queryset=None, *args, **kwargs):
        super().__init__(child_relation=serializers.PrimaryKeyRelatedField(queryset=queryset))


class DataSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        data = {k: None for k in self._fields.keys()}
        data.update(validated_data)
        return namedtuple("Data", data.keys())(*data.values())

    def data(self):
        self.is_valid(raise_exception=True)
        return self.save()


class DateSerializer(DataSerializer):
    date = serializers.DateField(required=False)


class FromToSerializer(DataSerializer):
    from_date = serializers.DateField(required=False)
    to_date = serializers.DateField(required=False)


class LocationSerializer(DataSerializer):
    longitude = serializers.FloatField()
    latitude = serializers.FloatField()


class UsernameSerializer(DataSerializer):
    username = serializers.CharField(required=False)


def image_sizes(picture, request):
    try:
        path = picture['thumbnail'].url
        if request:
            thumbnail = path
        else:
            thumbnail = path
    except:
        thumbnail = None

    try:
        path = picture['midsize'].url
        if request:
            midsize = path
        else:
            midsize = path
    except:
        midsize = None

    try:
        path = picture['fullsize'].url
        if request:
            fullsize = path
        else:
            fullsize = path
    except:
        fullsize = None

    try:
        path = picture.url
        if request:
            file = picture.url
        else:
            file = path
    except ValueError:
        file = None

    ####################################################################################################################
    # Modificacion de MARIO. Incluyo esto para asegurarnos de que devolvemos la URL completa
    # Las modificaciones son lo que esta aislado por ####, si se quita este fragmento tenemos el codigo original
    ####################################################################################################################
    if request.is_secure():
        header = 'https://'
    else:
        header = 'http://'
    header = 'https://'
    host = request.META['HTTP_HOST']
    if file is not None:
        file = header + host + file
    if thumbnail is not None:
        thumbnail = header + host + thumbnail
    if midsize is not None:
        midsize = header + host + midsize
    if fullsize is not None:
        fullsize = header + host + fullsize
    ####################################################################################################################

    return {
        'file': file,
        'thumbnail': thumbnail,
        'midsize': midsize,
        'fullsize': fullsize
    }