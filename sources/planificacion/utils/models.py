import math
import os
import uuid
from functools import wraps
from uuid import uuid4

from django.core.validators import RegexValidator
from django.db import models
from django.utils.deconstruct import deconstructible

from planificacion.utils.date import tz_now

# from location_field.models.plain import PlainLocationField

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="El telefono debe corresponder al formato: '+999999999'.")


# Create models with created_at field
class AbstractCreatedAt(models.Model):
    created_at = models.DateTimeField('Fecha', default=tz_now)

    class Meta:
        abstract = True
        ordering = ['-created_at']


# Create models with enabled field
class AbstractEnabled(models.Model):
    enabled = models.BooleanField(default=True, verbose_name='Activado')

    class Meta:
        abstract = True


# Get uuid4 filename from given uoad_to
@deconstructible
class PathAndRename(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)


# Create models with automated deletable file fields
class DeletableFileFieldsModel(models.Model):
    def __init__(self, *args, **kwargs):
        super(DeletableFileFieldsModel, self).__init__(*args, **kwargs)
        for field in self.deletable_fields:
            try:
                setattr(self, '__original_%s' % field, getattr(self, field))
            except AttributeError:
                pass

    def save(self, *args, **kwargs):
        for field in self.deletable_fields:
            try:
                orig = getattr(self, '__original_%s' % field)
                if orig != getattr(self, field):
                    try:
                        os.remove(orig.path)
                    except ValueError:
                        pass
                    except FileNotFoundError:
                        pass
            except AttributeError:
                pass
        super(DeletableFileFieldsModel, self).save(*args, **kwargs)

    def delete(self, **kwargs):
        for field in self.deletable_fields:
            try:
                field_instance = getattr(self, field)
                field_instance.delete(save=False)
            except AttributeError:
                pass
        super(DeletableFileFieldsModel, self).delete(**kwargs)

    class Meta:
        abstract = True


# Can be replaced by ArrayField see https://docs.djangoproject.com/en/1.10/ref/contrib/postgres/fields/
class SeparatedValuesField(models.TextField):
    def __init__(self, *args, **kwargs):
        self.token = kwargs.pop('token', ',')
        super(SeparatedValuesField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value: return
        if isinstance(value, list):
            return value
        return value.split(self.token)

    def get_prep_value(self, value):
        if not value: return
        assert(isinstance(value, list) or isinstance(value, tuple))
        return self.token.join([str(s) for s in value])

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_prep_value(value)

"""
class LocatedModel(models.Model):

    location = PlainLocationField(based_fields=['city'], zoom=7, verbose_name='Localización', blank=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default=0)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default=0)

    def save(self, *args, **kwargs):
        try:
            latitude, longitude = map(float, self.location.split(','))
        except ValueError:
            latitude = 0
            longitude = 0

        self.latitude, self.longitude = latitude, longitude

        super().save(*args, **kwargs)

    class Meta:
        abstract = True
"""

class BoundingCenter(object):
    def __init__(self, *args, **kwargs):
        self.lat_min = None
        self.lon_min = None
        self.lat_max = None
        self.lon_max = None

    @staticmethod
    def get_bounding_center(latitude_in_degrees, longitude_in_degrees, half_side_in_km):
        assert latitude_in_degrees >= -90.0 and latitude_in_degrees <= 90.0
        assert longitude_in_degrees >= -180.0 and longitude_in_degrees <= 180.0

        lat = math.radians(latitude_in_degrees)
        lon = math.radians(longitude_in_degrees)

        radius = 6371
        # Radius of the parallel at given latitude
        parallel_radius = radius * math.cos(lat)

        lat_min = lat - half_side_in_km / radius
        lat_max = lat + half_side_in_km / radius
        lon_min = lon - half_side_in_km / parallel_radius
        lon_max = lon + half_side_in_km / parallel_radius
        rad2deg = math.degrees

        center = BoundingCenter()
        center.lat_min = rad2deg(lat_min)
        center.lon_min = rad2deg(lon_min)
        center.lat_max = rad2deg(lat_max)
        center.lon_max = rad2deg(lon_max)

        return center



def hex_id_model(prefix):
    class _HexIdModel(models.Model):
        id = models.CharField(primary_key=True, editable=False, max_length=10, verbose_name='Id')

        def save(self, *args, **kwargs):
            if not self.id:
                while True:
                    self.id = prefix + uuid.uuid4().hex[:6].upper()
                    if not self.__class__.objects.filter(id=self.id).exists():
                        break
            super().save(*args, **kwargs)

        class Meta:
            abstract = True
    return _HexIdModel


def prevent_recursion(func):
    @wraps(func)
    def no_recursion(sender, instance=None, **kwargs):
        if not instance:
            return

        if hasattr(instance, '_dirty'):
            return

        func(sender, instance=instance, **kwargs)

        try:
            instance._dirty = True
            instance.save()
        finally:
            del instance._dirty
    return no_recursion
