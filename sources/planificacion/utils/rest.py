from collections import namedtuple

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet


class PaginationWithPositionInjection(PageNumberPagination):
    def get_paginated_response(self, data):
        for index, value in enumerate(data):
            value['position'] = self.page.paginator.per_page * (self.page.number - 1) + index + 1
        return super(PaginationWithPositionInjection, self).get_paginated_response(data)


# noinspection PyUnresolvedReferences
class PaginateWithSerializerModelMixin(object):
    def paginate_with_serializer(self, queryset, serializer_class, request):
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = serializer_class(page, many=True, context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = serializer_class(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class LocalizedGenericViewSet(GenericViewSet):
    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)

        location = self.request.query_params.get('location')

        try:
            self.request.latitude, self.request.longitude = map(float, location.split(','))
        except (ValueError, AttributeError):
            self.request.latitude, self.request.longitude = None, None
