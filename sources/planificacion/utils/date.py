# coding=utf-8
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
import pytz


def tz_now():
    return timezone.now().astimezone(pytz.timezone(settings.TIME_ZONE))


def tz_hour_ago():
    return tz_now() - timedelta(hours=1)


def tz_today():
    """ Today´s date given the timezone from settings file """
    return tz_now().date()


def tz_yesterday():
    """ Yesterday´s date given the timezone from settings file """
    return tz_today() - timedelta(days=1)


def tz_week_start(week_day=None):
    today_date = week_day or tz_today()
    return today_date - timedelta(days=today_date.weekday())


def tz_week_end(week_day=None):
    return tz_week_start(week_day) + timedelta(days=7)


def simple_format_date(date):
    return date.strftime('%b %-d %-I:%M %p')