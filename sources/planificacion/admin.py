
from django.contrib.admin import AdminSite


class MyAdminSite(AdminSite):
    site_title = site_header = 'planificacion'

    def get_urls(self):
        urls = super(MyAdminSite, self).get_urls()
        return urls


admin_site = MyAdminSite(name='my_admin')
