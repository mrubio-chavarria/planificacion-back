"""planificacion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from planificacion.admin import admin_site

urlpatterns = [
    path('admin/', admin_site.urls),
    url(r'^', include('services.users.urls')),
    url(r'^', include('services.configs.urls')),
    url(r'^', include('services.staff.urls')),
    url(r'^', include('services.projects.urls')),
    url(r'^auth/', include('rest_registration.api.urls', namespace='rest_framework_accounts')),
    path(r'auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^nested_admin/', include('nested_admin.urls')),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
