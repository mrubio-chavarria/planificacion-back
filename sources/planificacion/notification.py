import os

from planificacion.settings import send_notifications


class NotificationSenderError(Exception):
    pass


class NotificationSender:
    def __init__(self):
        self.notifications = {}

    def register(self, identifier, title, body, code):
        self.notifications[identifier] = (title, body, code)

    def unregister(self, indentifier):
        del self.notifications[indentifier]

    def send(self, users, identifier, **kwargs):
        if identifier not in self.notifications.keys():
            raise NotificationSenderError('Notification identifier not registered')

        title, body, code = self.notifications.get(identifier)
        data = kwargs.get('data', {})
        data = {'type': identifier, 'data': data}

        try:
            data['title'] = title = title.format(**kwargs)
            data['body'] = body = body.format(**kwargs)
        except KeyError as e:
            raise NotificationSenderError('{} Kwarg required to parse the content'.format(e))
        # PARTE DEL CÓDIGO QUE HAY QUE ADAPTAR
        """
        for user in users:
            print('code', code)
            if code is not None:
                user.hists.create(type=code, data=data)
            if not user.device_id:
                pass
            try:
                if user.platform == 'android':
                    send_notifications.notify_single_device(
                        registration_id=user.device_id,
                        content_available=True,
                        data_message=data
                    )
                else:
                    send_notifications.notify_single_device(
                        registration_id=user.device_id,
                        message_title=title,
                        message_body=body,
                        content_available=True,
                        data_message=data
                    )
            except:
                pass
        """

notification_sender = NotificationSender()

notification_sender.register(
    'new_offer',
    'Nueva oferta',
    'Hay una nueva oferta en uno de tus aeropuertos favoritos',
    0
)

notification_sender.register(
    'modificate_booking',
    'Reserva modificada',
    'Una reserva ha sido modificada',
    1
)

notification_sender.register(
    'cancelled_appointment',
    'Reserva cancelada',
    'Una reserva ha sido cancelada',
    2
)

notification_sender.register(
    'added_center',
    '{title}',
    '{body}',
    3
)

notification_sender.register(
    'close_service',
    'Servicio cercano',
    'Su servicio empezará en media hora',
    None
)
