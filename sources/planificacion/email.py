
from services.users.tasks import send_email


class EmailSenderError(Exception):
	pass


class EmailSender:
	def __init__(self):
		self.emails = {}

	def register(self, identifier: object, template: object, subject: object, content: object) -> object:
		self.emails[identifier] = (template, subject, content)

	def unregister(self, indentifier):
		del self.emails[indentifier]

	def send(self, receiver, identifier, **kwargs):
		if identifier not in self.emails.keys():
			raise EmailSenderError('Email identifier not registered')

		template, subject, content = self.emails.get(identifier)
		try:
			content = content.format(**kwargs)
		except KeyError as e:
			raise EmailSenderError('{} Kwarg required to parse the content'.format(e))
		send_email(receiver, template, {'subject': subject, 'content': content})


email_sender = EmailSender()

email_sender.register(
	'new_password',
	'plain',
	'Aquí tienes tu nueva contraseña',
	'Tu nueva contraseña es {password}'
)

email_sender.register(
	'contact',
	'plain',
	'Mensaje de contacto',
	'{text}'
)