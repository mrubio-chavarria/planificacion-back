from django.conf.urls import url


from services.configs import views as configs


urlpatterns = [
	url(r'^configs/update_required/$', configs.update_required),
]