from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from services.configs.models import AppVersion


@api_view(['POST'])
@permission_classes([AllowAny])
def update_required(request):
    version, platform = request.data.get('version'), request.data.get('platform')
    return Response({'detail': AppVersion.objects.filter(platform=platform, name__gt=version, required=True).exists()})
