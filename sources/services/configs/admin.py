from django.contrib import admin

# Register your models here.
from planificacion.admin import admin_site
from services.configs.models import AppVersion


class AppVersionAdmin(admin.ModelAdmin):
    list_display = ('name', 'platform', 'required')
    list_filter = ('platform', 'required')


admin_site.register(AppVersion, AppVersionAdmin)