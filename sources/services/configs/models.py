from django.db import models

# Create your models here.


class AppVersion(models.Model):
    IOS = 'ios'
    ANDROID = 'android'
    PLATFORMS = (
        (IOS, 'Ios'),
        (ANDROID, 'Android'),
    )
    platform = models.CharField(max_length=15, choices=PLATFORMS, default=IOS, verbose_name='Plataforma')
    name = models.CharField(max_length=15, verbose_name='Nombre')
    required = models.BooleanField(default=False, verbose_name='Requerida')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Versión de App'
        verbose_name_plural = 'Versiones de app'
