
from rest_framework import serializers
from .models import Plan, Project
from services.staff.serializers import PersonSerializer


class PlanBaseSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model Plan.
    """

    class Meta:
        fields = ['id', 'project', 'person', 'date']
        model = Plan


class ProjectSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model Project.
    """

    class Meta:
        fields = ['id', 'name', 'comments', 'state', 'isDesigned', 'isDevelopment', 'manager', 'start_date',
                  'planned_end_date', 'real_end_date']
        model = Project


class ProjectLoadSerializer(ProjectSerializer):
    """
    DESCRIPTION:
    Serializer to model Project.
    """

    # Attributes
    associated_plans = PlanBaseSerializer(many=True)

    class Meta(ProjectSerializer.Meta):
        fields = ['id', 'name', 'comments', 'state', 'isDesigned', 'isDevelopment', 'manager', 'start_date',
                  'planned_end_date', 'real_end_date', 'associated_plans']


class PlanDetailSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model Plan with nested serializers for project and person.
    """

    # Attributes
    project = ProjectSerializer()
    person = PersonSerializer()

    class Meta:
        fields = ['id', 'project', 'person', 'date']
        model = Plan