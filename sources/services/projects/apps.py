
from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'services.projects'
    verbose_name = 'projects'

