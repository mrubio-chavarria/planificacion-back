
from django.db import models
from planificacion.utils.models import hex_id_model
from services.staff.models import Person, Group


# Create your models here.

class Project(hex_id_model('PJ'), models.Model):
    """
    DESCRIPTION:
    Model to represent the information of a project.
    """

    # Attributes
    name = models.CharField(max_length=200, blank=True, null=True, verbose_name='Nombre')
    comments = models.CharField(max_length=200, blank=True, null=True, verbose_name='Comentarios')
    ACTIVE = 0
    PAUSE = 1
    INACTIVE = 2
    STATES = (
        (ACTIVE, 'Activo'),
        (PAUSE, 'Pausa'),
        (INACTIVE, 'Inactivo')
    )
    state = models.IntegerField(choices=STATES, default=1, verbose_name='Estado')
    isDesigned = models.BooleanField(default=False)
    isDevelopment = models.BooleanField(default=False)
    manager = models.ForeignKey(Person, on_delete=models.SET_NULL, blank=True, null=True,
                                related_name='managed_projects', verbose_name='Manager')
    start_date = models.DateTimeField(null=True, blank=True, verbose_name='Fecha de inicio')
    planned_end_date = models.DateTimeField(null=True, blank=True, verbose_name='Fecha planeada de finalización')
    real_end_date = models.DateTimeField(null=True, blank=True, verbose_name='Fecha real de finalización')

    # Methods
    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyectos'


class Plan(hex_id_model('PL'), models.Model):
    """
    DESCRIPTION:
    A model to set the object of every day plans.
    """

    # Attributes
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='plans',
                               verbose_name='Persona')
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='plans',
                                verbose_name='Proyecto')
    date = models.DateTimeField(null=True, blank=True, verbose_name='Fecha')

    # Methods
    def __str__(self):
        return f'Person: {self.person.name if self.person is not None else "None"}' \
               f' Project: {self.project.name if self.project is not None else "None"}'

    class Meta:
        verbose_name = 'Plan'
        verbose_name_plural = 'Planes'


class ProjectPersonRelation(hex_id_model('PP'), models.Model):
    """
    DESCRIPTION:
    A model to establish the many to many relation between projects and persons.
    """

    # Attributes
    project = models.ForeignKey(Project, on_delete=models.CASCADE, blank=True, null=True,
                                related_name='persons_relations', verbose_name='Proyecto')
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True,
                               related_name='projects_relations', verbose_name='Persona')

    # Methods
    def __str__(self):
        return f'Project: {self.project.name} Person: {self.person.name}'

    class Meta:
        verbose_name = 'Relación Proyecto/Persona'
        verbose_name_plural = 'Relaciones Proyecto/Persona'


class ProjectGroupRelation(hex_id_model('PG'), models.Model):
    """
    DESCRIPTION:
    A model to establish the many to many relation between projects and groups.
    """

    # Attributes
    project = models.ForeignKey(Project, on_delete=models.CASCADE, blank=True, null=True,
                                related_name='groups_relations', verbose_name='Proyecto')
    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True,
                              related_name='projects_relations', verbose_name='Grupo')

    # Methods
    def __str__(self):
        return f'Project: {self.project.name} Group: {self.group.name}'

    class Meta:
        verbose_name = 'Relación Proyecto/Grupo'
        verbose_name_plural = 'Relaciones Proyecto/Grupo'
