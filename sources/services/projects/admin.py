
from django.contrib import admin
from planificacion.admin import admin_site
from .models import Project, Plan


class ProjectAdmin(admin.ModelAdmin):
    readonly_fields = ('id', )


class PlanAdmin(admin.ModelAdmin):
    readonly_fields = ('id', )


admin_site.register(Project, ProjectAdmin)
admin_site.register(Plan, PlanAdmin)

