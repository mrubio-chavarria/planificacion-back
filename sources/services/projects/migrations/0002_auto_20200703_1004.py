# Generated by Django 2.0.6 on 2020-07-03 08:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('staff', '0002_auto_20200703_1004'),
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Plan',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
                ('date', models.DateTimeField(blank=True, null=True, verbose_name='Fecha')),
            ],
            options={
                'verbose_name': 'Plan',
                'verbose_name_plural': 'Planes',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Nombre')),
                ('comments', models.CharField(blank=True, max_length=200, null=True, verbose_name='Comentarios')),
                ('state', models.IntegerField(choices=[(0, 'Activo'), (1, 'Pausa'), (2, 'Inactivo')], default=1, verbose_name='Estado')),
                ('isDesigned', models.BooleanField(default=False)),
                ('isDevelopment', models.BooleanField(default=False)),
                ('start_date', models.DateTimeField(blank=True, null=True, verbose_name='Fecha de inicio')),
                ('planned_end_date', models.DateTimeField(blank=True, null=True, verbose_name='Fecha planeada de finalización')),
                ('real_end_date', models.DateTimeField(blank=True, null=True, verbose_name='Fecha real de finalización')),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
            },
        ),
        migrations.CreateModel(
            name='ProjectGroupRelation',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
            ],
            options={
                'verbose_name': 'Relación Proyecto/Grupo',
                'verbose_name_plural': 'Relaciones Proyecto/Grupo',
            },
        ),
        migrations.CreateModel(
            name='ProjectPersonRelation',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
            ],
            options={
                'verbose_name': 'Relación Proyecto/Persona',
                'verbose_name_plural': 'Relaciones Proyecto/Persona',
            },
        ),
        migrations.DeleteModel(
            name='Group',
        ),
        migrations.RemoveField(
            model_name='grouppersonrelation',
            name='group',
        ),
        migrations.RemoveField(
            model_name='grouppersonrelation',
            name='person',
        ),
        migrations.DeleteModel(
            name='Person',
        ),
        migrations.DeleteModel(
            name='GroupPersonRelation',
        ),
        migrations.AddField(
            model_name='projectpersonrelation',
            name='person',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='projects_relations', to='staff.Person', verbose_name='Persona'),
        ),
        migrations.AddField(
            model_name='projectpersonrelation',
            name='project',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='persons_relations', to='projects.Project', verbose_name='Proyecto'),
        ),
        migrations.AddField(
            model_name='projectgrouprelation',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='projects_relations', to='staff.Group', verbose_name='Grupo'),
        ),
        migrations.AddField(
            model_name='projectgrouprelation',
            name='project',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='groups_relations', to='projects.Project', verbose_name='Proyecto'),
        ),
        migrations.AddField(
            model_name='project',
            name='manager',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='managed_projects', to='staff.Person', verbose_name='Manager'),
        ),
        migrations.AddField(
            model_name='plan',
            name='person',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='plans', to='staff.Person', verbose_name='Persona'),
        ),
        migrations.AddField(
            model_name='plan',
            name='project',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='plans', to='projects.Project', verbose_name='Proyecto'),
        ),
    ]
