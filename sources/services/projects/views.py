
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from .models import Project, Plan
from .serializers import ProjectSerializer, ProjectLoadSerializer, PlanBaseSerializer
from services.staff.serializers import PersonLoadSerializer
from services.staff.models import Person


class ProjectViewSet(viewsets.ModelViewSet):

    # Attributes
    queryset = Project.objects.all().order_by('id')
    serializer_class = ProjectSerializer
    permission_classes = (IsAuthenticated, )


class PlanViewSet(viewsets.ModelViewSet):

    # Attributes
    queryset = Plan.objects.all().order_by('id')
    serializer_class = PlanBaseSerializer
    permission_classes = (IsAuthenticated, )

    # Methods
    @action(detail=False, methods=['GET', ])
    def filtered_plans(self, request):
        """
        DESCRIPTION:
        The view to return the plans filtered by day, person or project.
        """
        # Filtering
        params = request.GET
        queryset = self.queryset
        if 'dates' in params.keys() and params.get('dates') is not None:
            queryset = queryset.filter(date__in=params.get('dates'))
        if 'persons' in params.keys() and params.get('persons') is not None:
            queryset = queryset.filter(person__id__in=params.get('persons'))
        if 'projects' in params.keys() and params.get('projects') is not None:
            queryset = queryset.filter(project__id__in=params.get('projects').split(','))
        # Grouping
        parameter = params.get('group_by') if 'group_by' in params.keys() and 'group_by' is not None else 'project'
        bases = group_by(queryset, parameter)
        # Serializing
        if parameter == 'project':
            serializer = ProjectLoadSerializer(bases, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        elif parameter == 'person':
            serializer = PersonLoadSerializer(bases, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            message = [{base[0]: PlanBaseSerializer(base[1], many=True).data} for base in bases]
            return Response(message, status=status.HTTP_200_OK)


def group_by(queryset, parameter='project'):
    """
    DESCRIPTION:
    A function to group by the specified parameter.
    :param queryset: [queryset] the list of elements to be grouped.
    :param parameter: [string] the indication to center the grouping.
    :return: [list] objects with the associated plans.
    """
    # Obtain the groups
    if parameter != 'date':
        groups = {group: [plan for plan in queryset if getattr(plan, parameter).id == group]
                  for group in queryset.values_list(parameter, flat=True).distinct()}
    else:
        groups = {str(group).split(' ')[0]: [plan for plan in queryset
                                             if str(getattr(plan, parameter)).split(' ')[0] == str(group).split(' ')[0]]
                  for group in queryset.values_list(parameter, flat=True).distinct()}
    # Obtain the objects to which the plans are to be set
    if parameter == 'project':
        bases = Project.objects.filter(id__in=groups.keys())
        [setattr(base, 'associated_plans', groups[base.id]) for base in bases]
    elif parameter == 'person':
        bases = Person.objects.filter(id__in=groups.keys())
        [setattr(base, 'associated_plans', groups[base.id]) for base in bases]
    else:
        bases = groups.items()
    return bases

