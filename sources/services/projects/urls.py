from django.conf.urls import url, include
from rest_framework import routers
from . import views as projects

router = routers.SimpleRouter()
router.register('projects', projects.ProjectViewSet , base_name='projects')
router.register('plans', projects.PlanViewSet, base_name='plans')


urlpatterns = [
    url(r'^', include(router.urls)),
]
