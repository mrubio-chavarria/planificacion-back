
import uuid
from rest_framework import mixins, status, exceptions
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from services.users.models import User
from services.users.serializers import EmailSerializer, UserSerializer
from planificacion.email import email_sender
from planificacion.utils.rest import PaginateWithSerializerModelMixin


class UserViewSet(mixins.UpdateModelMixin, mixins.RetrieveModelMixin, mixins.ListModelMixin, GenericViewSet,
                  mixins.DestroyModelMixin, PaginateWithSerializerModelMixin):
    """
    DESCRIPTION:
    Viewset of user-related views.
    """
    permission_classes = (IsAuthenticated,)

    # Parameters
    serializer_class = UserSerializer

    # Methods
    @action(detail=False, methods=['get'])
    def me(self, request):
        return Response(UserSerializer(request.user, context={'request': request}).data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post', ], permission_classes=(AllowAny,))
    def exist_user(self, request):
        email = self.request.data.get('email')
        response = User.objects.filter(username=email).exists()

        return Response({'detail': response})

    @action(detail=False, methods=['post'])
    def password(self, request):
        old = request.data.get('old_password')
        new = request.data.get('new_password')

        if not old or not new:
            return Response('Se necesitan tanto la vieja como la nueva contraseña', status=status.HTTP_400_BAD_REQUEST)

        if not request.user.check_password(old):
            return Response({"old_password": "La contraseña no coincide"}, status=status.HTTP_400_BAD_REQUEST)

        request.user.set_password(new)
        request.user.save()

        return Response('Contraseña cambiada')

    @action(detail=False, methods=['post'], permission_classes=(AllowAny,))
    def new_password(self, request):
        data = EmailSerializer(data=request.data).data()
        try:
            user = User.objects.get(email__iexact=data.email)
        except User.DoesNotExist:
            return Response('Usuario no encontrado', status=status.HTTP_404_NOT_FOUND)

        password = uuid.uuid4().hex[:8].upper()
        user.set_password(password)
        user.save()
        email_sender.send(data.email, 'new_password', email=data.email, password=password)
        return Response('Contraseña cambiada')


def handle_exception(self, exc):
    """
    Handle any exception that occurs, by returning an appropriate response,
    or re-raising the error.
    """
    if isinstance(exc, (exceptions.NotAuthenticated,
                        exceptions.AuthenticationFailed)):
        # WWW-Authenticate header for 401 responses, else coerce to 403
        auth_header = self.get_authenticate_header(self.request)

        if auth_header:
            exc.auth_header = auth_header

    exception_handler = self.get_exception_handler()

    context = self.get_exception_handler_context()
    response = exception_handler(exc, context)

    if response is None:
        self.raise_uncaught_exception(exc)

    response.exception = True
    return response


APIView.handle_exception = handle_exception
