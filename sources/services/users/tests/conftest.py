import pytest

from services.users.models import User, Position


@pytest.fixture
def position():
    position = Position.objects.create(name='Manager')
    position.save()
    return position