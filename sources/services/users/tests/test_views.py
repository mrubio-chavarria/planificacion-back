import pytest
from services.users.models import User

@pytest.mark.django_db
class TestUsers:

    def test_me(self, logged_client, user):
        response = logged_client.get('/users/me/')
        assert response.status_code == 200
        assert response.json()['id'] == user.id


    def test_exist(self, logged_client, position):
        data = {
            'email': 'celia@rudo.es'
        }
        response = logged_client.post('/users/exist_user/', data)
        assert response.status_code == 200
