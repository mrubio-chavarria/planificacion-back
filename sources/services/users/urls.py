
from django.conf.urls import url, include
from rest_framework import routers
from services.users import admin_autocomplete

# Register API Endpoints
from services.users import views as users

router = routers.SimpleRouter()
router.register('users', users.UserViewSet, base_name='users')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^user-autocomplete/$', admin_autocomplete.UserAutocomplete.as_view(), name='user-autocomplete'),
    # path(r'auth/token/', users.UserViewSet.login)
]
