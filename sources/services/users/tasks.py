
from __future__ import absolute_import, unicode_literals
from planificacion import settings
from templated_email import send_templated_mail


def send_email(email, template, context):
    print(email, template, context)
    if email is None or email == '':
        return

    send_templated_mail(
        template_name=template,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[email],
        context=context
    )

