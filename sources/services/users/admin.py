
from planificacion.admin import admin_site
from django.contrib.auth.admin import UserAdmin
from oauth2_provider.admin import ApplicationAdmin
from oauth2_provider.models import Application
from django.utils.translation import gettext_lazy as _
from services.users.models import User


class CustomUserAdmin(UserAdmin):
    readonly_fields = ('id', )
    fieldsets = (
        (None, {'fields': ('id', 'id_agora', 'is_active', 'is_superuser', 'password',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'country', 'email')}),
    )


admin_site.register(Application, ApplicationAdmin)
admin_site.register(User, CustomUserAdmin)

