
from rest_framework import serializers
from planificacion.utils.serializers import DataSerializer
from services.users.models import User


class UserMinSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'device_id')


class EmailSerializer(DataSerializer):
    email = serializers.CharField()


class ModifiedEmailSerializer(DataSerializer):
    email = serializers.CharField()
    text = serializers.CharField()


class TokenSerializer(DataSerializer):
    client_id = serializers.CharField()
    client_secret = serializers.CharField()
    # token = serializers.CharField()
