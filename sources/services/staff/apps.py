
from django.apps import AppConfig


class StaffConfig(AppConfig):
    name = 'services.staff'
    verbose_name = 'staff'

