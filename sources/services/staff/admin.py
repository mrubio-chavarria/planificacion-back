
from django.contrib import admin
from planificacion.admin import admin_site
from .models import Person, Group, GroupPersonRelation


class PersonAdmin(admin.ModelAdmin):
    readonly_fields = ('id', )


class GroupAdmin(admin.ModelAdmin):
    readonly_fields = ('id', )


class GroupPersonRelationAdmin(admin.ModelAdmin):
    readonly_fields = ('id', )


admin_site.register(Person, PersonAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(GroupPersonRelation, GroupPersonRelationAdmin)

