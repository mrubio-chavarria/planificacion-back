from django.conf.urls import url, include
from rest_framework import routers
from . import views as staff

router = routers.SimpleRouter()
router.register('persons', staff.PersonViewSet, base_name='persons')
router.register('groups', staff.GroupViewSet, base_name='groups')


urlpatterns = [
    url(r'^', include(router.urls)),
]
