
from django.db import models
from planificacion.utils.models import hex_id_model


# Create your models here.

class Group(hex_id_model('GP'), models.Model):
    """
    DESCRIPTION:
    Model to group the persons in work sections.
    """

    # Attributes
    name = models.CharField(max_length=200, null=True, blank=True, verbose_name='nombre')

    # Methods
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'


class Person(hex_id_model('PR'), models.Model):
    """
    DESCRIPTION:
    Model to store the persons who participate in the projects.
    """

    # Attributes
    name = models.CharField(max_length=200, null=True, blank=True, verbose_name='Nombre')
    email = models.CharField(max_length=200, null=True, blank=True, verbose_name='Correo')

    # Methods
    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'


class GroupPersonRelation(hex_id_model('GR'), models.Model):
    """
    DESCRIPTION:
    Model to make the many to many relation between persons and groups.
    """

    # Attributes
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='relations', blank=True, null=True,
                              verbose_name='grupo')
    person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='relations', blank=True, null=True,
                               verbose_name='persona')

    # Models
    def __str__(self):
        return f'group: {str(self.group.name)} person: {str(self.person.name)}'

    class Meta:
        verbose_name = 'Relación Grupo/Persona'
        verbose_name_plural = 'Relaciones Grupo/Persona'

