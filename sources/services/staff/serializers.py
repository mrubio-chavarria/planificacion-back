
from rest_framework import serializers
from .models import Person, Group
from services.projects.models import Plan


class PersonSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model person.
    """

    class Meta:
        fields = ['id', 'name', 'email']
        model = Person


class PlanBaseSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model Plan.
    """

    class Meta:
        fields = ['id', 'project', 'person', 'date']
        model = Plan


class PersonLoadSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model person.
    """
    # Attributes
    associated_plans = PlanBaseSerializer(many=True)

    class Meta:
        fields = ['id', 'name', 'email', 'associated_plans']
        model = Person


class GroupSerializer(serializers.ModelSerializer):
    """
    DESCRIPTION:
    Serializer to model group.
    """

    class Meta:
        fields = ['id', 'name']
        model = Group

