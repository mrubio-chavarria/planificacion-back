
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .models import Person, Group
from .serializers import PersonSerializer, GroupSerializer


class PersonViewSet(viewsets.ModelViewSet):

    # Attributes
    queryset = Person.objects.all().order_by('id')
    serializer_class = PersonSerializer
    permission_classes = (IsAuthenticated, )


class GroupViewSet(viewsets.ModelViewSet):

    # Attributes
    queryset = Group.objects.all().order_by('id')
    serializer_class = GroupSerializer
    permission_classes = (IsAuthenticated, )

