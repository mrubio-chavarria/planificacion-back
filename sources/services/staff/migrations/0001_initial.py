# Generated by Django 2.0.6 on 2020-07-03 06:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
                ('name', models.CharField(default='default_group_name', max_length=200, verbose_name='nombre')),
            ],
            options={
                'verbose_name': 'Grupo',
                'verbose_name_plural': 'Grupos',
            },
        ),
        migrations.CreateModel(
            name='GroupPersonRelation',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
                ('group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='relations', to='staff.Group', verbose_name='grupo')),
            ],
            options={
                'verbose_name': 'Relación Grupo/Persona',
                'verbose_name_plural': 'Relaciones Grupo/Persona',
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.CharField(editable=False, max_length=10, primary_key=True, serialize=False, verbose_name='Id')),
                ('name', models.CharField(default='default_person_name', max_length=200, verbose_name='nombre')),
                ('email', models.CharField(default='default_email', max_length=200, verbose_name='correo')),
            ],
            options={
                'verbose_name': 'Persona',
                'verbose_name_plural': 'Personas',
            },
        ),
        migrations.AddField(
            model_name='grouppersonrelation',
            name='person',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='relations', to='staff.Person', verbose_name='persona'),
        ),
    ]
