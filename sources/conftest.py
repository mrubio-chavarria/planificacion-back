import pytest
from services.users.models import User

@pytest.fixture
def user(position):
    user = User.objects.create(email='test_user@rudo.es', username='test_user@rudo.es', id_agora='1', position=position)
    user.set_password('12345678A')
    user.save()
    return user


@pytest.fixture
def logged_client(client, user):
    client.login(email='test_user@rudo.es', username='test_user@rudo.es', password='12345678A', id_agora='00001')

    return client
